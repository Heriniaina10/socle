﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Socle.Filters;
using Socle.Models.Do;

namespace Socle.Extensions
{
    // ReSharper disable once InconsistentNaming
    public static class IQueryableExtension
    {
        public static IQueryable<T> ApplyOrdering<T>(this IQueryable<T> query, ISorting queryObj, Dictionary<string, Expression<Func<T, object>>> columnsMap)
        {
            if (string.IsNullOrEmpty(queryObj.SortBy) || !columnsMap.ContainsKey(queryObj.SortBy))
            {
                return query;
            }


            if (queryObj.IsSortAscending.GetValueOrDefault())
            {
                return query.OrderBy(columnsMap[queryObj.SortBy]);
            }

            return query.OrderByDescending(columnsMap[queryObj.SortBy]);
        }

        public static IQueryable<T> ApplyPaging<T>(this IQueryable<T> query, ISorting queryObj)
        {
            if (queryObj.Page <= 0)
            {
                queryObj.Page = 1;
            }
            if (queryObj.PageSize <= 0)
            {
                queryObj.PageSize = 10;
            }

            return query.Skip((queryObj.Page - 1) * queryObj.PageSize)
                .Take(queryObj.PageSize);
        }
    }
}
