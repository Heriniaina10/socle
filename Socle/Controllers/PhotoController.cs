﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Socle.Models.Do;
using Socle.Models.Dto;
using Socle.Repositories;
using Socle.Repositories.Photo;
using Socle.Services.Photo;
using Socle.UO;

namespace Socle.Controllers
{
    [Route("api/vehicle/{vehicleId}/[controller]")]
    [ApiController]
    public class PhotoController : ControllerBase
    {
        private readonly IHostingEnvironment _host;
        private readonly IVehicleRepository _vehicleRepository;
        private readonly IPhotoRepository _photoRepository;
        private readonly IMapper _mapper;
        private readonly IPhotoService _photoService;
        private readonly PhotoSettings _settings;

        public PhotoController(
            IHostingEnvironment host, 
            IVehicleRepository vehicleRepository, 
            IPhotoRepository photoRepository, 
            IMapper mapper, 
            IOptionsSnapshot<PhotoSettings> options,
            IPhotoService photoService)
        {
            _host = host;
            _vehicleRepository = vehicleRepository;
            _photoRepository = photoRepository;
            _mapper = mapper;
            _photoService = photoService;
            _settings = options.Value;
        }

        [HttpGet]
        public async Task<IEnumerable<PhotoDto>> GetPhotos(int vehicleId)
        {
            var photos = await _photoRepository.GetPhotos(vehicleId);

            return _mapper.Map<IEnumerable<Photo>, IEnumerable<PhotoDto>>(photos);
        }

        [HttpPost]
        public async Task<IActionResult> Upload(int vehicleId, IFormFile file)
        {
            var vehicle = await _vehicleRepository.GetVehicle(vehicleId, false);
            if (vehicle == null)
            {
                return NotFound();
            }

            if (file == null) return BadRequest("Null file.");
            if (file.Length == 0) return BadRequest("Empty file.");

            if (file.Length > _settings.MaxFileSize) return BadRequest("Max file size exceed.");
            if (_settings.IsSupported(file.FileName)) return BadRequest("Invalid file type.");

            var folderPath = Path.Combine(_host.WebRootPath, "UPLOADS");
            var photo = await _photoService.Upload(vehicle, file, folderPath);

            return Ok(_mapper.Map<Photo, PhotoDto>(photo));
        }
    }
}