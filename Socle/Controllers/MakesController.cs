﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Socle.Models.Do;
using Socle.Models.Dto;
using Socle.Persistence;

namespace Socle.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MakesController : ControllerBase
    {
        private readonly SocleDbContext _context;
        private readonly IMapper _mapper;

        public MakesController(SocleDbContext ctx, IMapper mapper)
        {
            _context = ctx;
            _mapper = mapper;
        }

        public async Task<IEnumerable<MakeDto>> GetMakes()
        {
            var res = await _context.Makes
                .Include(m => m.Models)
                .ToListAsync();

            return _mapper.Map<List<Make>, List<MakeDto>>(res);
        }
    }
}