﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Socle.Filters;
using Socle.Models.Do;
using Socle.Models.Dto;
using Socle.Persistence;
using Socle.Repositories;
using Socle.UO;

namespace Socle.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehicleController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IVehicleRepository _repository;
        private readonly IUnitOfWork _unitOfWork;

        public VehicleController(IMapper mapper, IVehicleRepository repository, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _repository = repository;
            _unitOfWork = unitOfWork;
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] SaveDto saveDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //var model = await _context.Models.FindAsync(saveDto.ModelId);
            //if (model == null)
            //{
            //    ModelState.AddModelError("ModelId", "Invalid modelId");

            //    return BadRequest(ModelState);
            //}

            var vehicle = _mapper.Map<SaveDto, Vehicle>(saveDto);
            vehicle.LastUpdate = DateTime.Now;

            _repository.Add(vehicle);
            await _unitOfWork.CompleteAsync();

            var result = await _repository.GetVehicle(vehicle.Id);

            return Ok(_mapper.Map<Vehicle, VehicleDto>(result));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] SaveDto saveDto)
        {
            var vehicle = await _repository.GetVehicle(id);
            _mapper.Map<SaveDto, Vehicle>(saveDto, vehicle);
            vehicle.LastUpdate = DateTime.Now;

           _repository.Update(vehicle);
            await _unitOfWork.CompleteAsync();

            var result = await _repository.GetVehicle(vehicle.Id);

            return Ok(_mapper.Map<Vehicle, SaveDto>(result));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var vehicle = await _repository.GetVehicle(id, false);
            if (vehicle == null)
            {
                return NotFound();
            }

            _repository.Delete(vehicle);
            await _unitOfWork.CompleteAsync();

            return Ok(id);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetVehicle(int id)
        {
            var vehicle = await _repository.GetVehicle(id);

            if (vehicle == null)
            {
                return NotFound();
            }

            var result = _mapper.Map<Vehicle, VehicleDto>(vehicle);

            return Ok(result);
        }

        [HttpGet]
        public async Task<QueryResult<VehicleDto>> GetVehicles(string SortBy, bool? IsSortAscending, int Page, int PageSize)
        {
            var filter = new Filter
            {
                SortBy = SortBy,
                IsSortAscending = IsSortAscending,
                Page = Page,
                PageSize = PageSize
            };

            //var filter = _mapper.Map<FilterDto, Filter>(filterDto);
            var aVehicles = await _repository.GetVehicles(filter);
            var result = _mapper.Map<QueryResult<Vehicle>, QueryResult<VehicleDto>>(aVehicles);

            return result;
        }
    }
}