﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Socle.Filters
{
    public interface ISorting
    {
        string SortBy { get; set; }
        bool? IsSortAscending { get; set; }

        int Page { get; set; }
        int PageSize { get; set; }
    }
}
