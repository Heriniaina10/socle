﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Socle.Persistence;

namespace Socle.UO
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly SocleDbContext _context;

        public UnitOfWork(SocleDbContext context)
        {
            _context = context;
        }

        public async Task CompleteAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}
