﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Socle.Models.Do;

namespace Socle.Services.Photo
{
    public interface IPhotoService
    {
        Task<Models.Do.Photo> Upload(Vehicle vehicle, IFormFile file, string path);
        Task<string> ManageAndSaveFile(string folderPath, IFormFile file);
    }
}
