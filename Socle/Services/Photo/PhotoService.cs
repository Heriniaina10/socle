﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Socle.Models.Do;
using Socle.Models.Dto;
using Socle.UO;

namespace Socle.Services.Photo
{
    public class PhotoService : IPhotoService
    {
        private readonly IUnitOfWork _uof;

        public PhotoService(IUnitOfWork uof)
        {
            _uof = uof;
        }

        public async Task<string> ManageAndSaveFile(string folderPath, IFormFile file)
        {
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }

            var fileName = Guid.NewGuid() + Path.GetExtension(file.FileName).ToLower();
            var filePath = Path.Combine(folderPath, fileName);
            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }

            return fileName;
        }

        public async Task<Models.Do.Photo> Upload(Vehicle vehicle, IFormFile file, string folderPath)
        {
            var fileName = await ManageAndSaveFile(folderPath, file);
            var photo = new Models.Do.Photo { FileName = fileName };
            vehicle.Photos.Add(photo);

            await _uof.CompleteAsync();

            return photo;
        }
    }
}
