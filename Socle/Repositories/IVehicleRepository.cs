﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Socle.Filters;
using Socle.Models.Do;

namespace Socle.Repositories
{
    public interface IVehicleRepository
    {
        Task<Vehicle> GetVehicle(int id, bool includeRelation = true);
        Task<QueryResult<Vehicle>> GetVehicles(Filter queryObj);
        void Add(Vehicle vehicle);
        void Delete(Vehicle vehicle);
        void Update(Vehicle vehicle);
    }
}
