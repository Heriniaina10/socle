﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Socle.Filters;
using Socle.Models.Do;

namespace Socle.Repositories.Photo
{
    public interface IPhotoRepository
    {
        //Task<Models.Do.Photo> GetPhoto(int vehicleId);
        Task<IEnumerable<Models.Do.Photo>> GetPhotos(int vehicleId);
    }
}
