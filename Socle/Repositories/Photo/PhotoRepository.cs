﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Socle.Persistence;

namespace Socle.Repositories.Photo
{
    public class PhotoRepository : IPhotoRepository
    {
        private readonly SocleDbContext _context;

        public PhotoRepository(SocleDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Models.Do.Photo>> GetPhotos(int vehicleId)
        {
            return await _context.Photos
                .Where(v => v.VehicleId == vehicleId)
                .ToListAsync();
        }
    }
}
