﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Socle.Extensions;
using Socle.Filters;
using Socle.Models.Do;
using Socle.Models.Dto;
using Socle.Persistence;

namespace Socle.Repositories
{
    public class VehicleRepository : IVehicleRepository 
    {
        private readonly SocleDbContext _context;

        public VehicleRepository(SocleDbContext context)
        {
            _context = context;
        }

        public async Task<Vehicle> GetVehicle(int id, bool includeRelation=true)
        {
            if (!includeRelation)
            {
                return await _context.Vehicles.FirstOrDefaultAsync(v => v.Id == id);
            }

            return await _context.Vehicles.Include(v => v.Features)
                .ThenInclude(vf => vf.Feature)
                .Include(m => m.Model)
                .ThenInclude(m => m.Make)
                .FirstOrDefaultAsync(v => v.Id == id);
        }

        public async Task<QueryResult<Vehicle>> GetVehicles(Filter queryObj)
        {
            var result = new QueryResult<Vehicle>();

            var query = _context.Vehicles.Include(v => v.Features)
                .ThenInclude(vf => vf.Feature)
                .Include(m => m.Model)
                .ThenInclude(m => m.Make)
                .AsQueryable();
            // Filter
            if (queryObj.MakeId.HasValue)
            {
                query = query.Where(v => v.Model.MakeId == queryObj.MakeId);
            }
            if (queryObj.ModelId.HasValue)
            {
                query = query.Where(v => v.ModelId == queryObj.ModelId);
            }

            // Sorting
            var columnsMap = new Dictionary<string, Expression<Func<Vehicle, object>>>
            {
                ["make"] = v => v.Model.Make.Name,
                ["model"] = v => v.Model.Name,
                ["contactName"] = v => v.ContactName,
                ["id"] = v => v.Id
            };
            query = query.ApplyOrdering(queryObj, columnsMap);
            result.Total = await query.CountAsync();
            // Paging
            query = query.ApplyPaging(queryObj);
            result.Items = await query.ToListAsync();
            
            return result;
        }

        public void Add(Vehicle vehicle)
        {
            _context.Vehicles.Add(vehicle);
        }

        public void Delete(Vehicle vehicle)
        {
            _context.Vehicles.Remove(vehicle);
        }

        public void Update(Vehicle vehicle)
        {
            _context.Vehicles.Update(vehicle);
        }

    }
}
