﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Socle.Filters;
using Socle.Models.Do;
using Socle.Models.Dto;

namespace Socle.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // DO to DTO
            CreateMap(typeof (QueryResult<>), typeof(QueryResultDto<>));
            CreateMap<Photo, PhotoDto>();
            CreateMap<Filter, FilterDto>();
            CreateMap<Make, MakeDto>();
            CreateMap<Make, KeyPairValueDto>();
            CreateMap<Model, ModelDto>();
            CreateMap<Feature, KeyPairValueDto>();
            CreateMap<Vehicle, SaveDto>()
                .ForMember(vr => vr.Contact, opt => opt.MapFrom(v => new ContactDto
                {
                    Name = v.ContactName,
                    Phone = v.ContactPhone,
                    Email = v.ContactEmail
                }))
                .ForMember(vr => vr.Features, opt => opt.MapFrom(v => v.Features.Select(vf => vf.FeatureId)));
            CreateMap<Vehicle, VehicleDto>()
                .ForMember(vr => vr.Contact, opt => opt.MapFrom(v => new ContactDto
                {
                    Name = v.ContactName,
                    Phone = v.ContactPhone,
                    Email = v.ContactEmail
                }))
                .ForMember(vr => vr.Features, opt => opt.MapFrom(v => v.Features.Select(vf => new KeyPairValueDto { Id = vf.Feature.Id, Name = vf.Feature.Name})))
                .ForMember(vr => vr.Make, opt => opt.MapFrom(v => v.Model.Make));

            // DTO to DO
            CreateMap<SaveDto, Vehicle>()
                .ForMember(v => v.Id, opt => opt.Ignore())
                .ForMember(v => v.ContactName, opt => opt.MapFrom(vr => vr.Contact.Name))
                .ForMember(v => v.ContactEmail, opt => opt.MapFrom(vr => vr.Contact.Email))
                .ForMember(v => v.ContactPhone, opt=> opt.MapFrom(vr => vr.Contact.Phone))
                .ForMember(v => v.Features, opt => opt.MapFrom(vr => vr.Features.Select(id => new VehicleFeature{FeatureId = id})))
                .ForMember(v => v.Features, opt => opt.Ignore())
                .AfterMap((vr, v) =>
                {
                    // Removed features
                    var removedFeatures = v.Features.Where(f => !vr.Features.Contains(f.FeatureId)).ToList();
                    foreach (var f in removedFeatures)
                    {
                        v.Features.Remove(f);
                    }

                    // Add new Feautres
                    var addedFeatures = vr.Features.Where(id => v.Features.All(f => f.FeatureId != id))
                        .Select(id => new VehicleFeature { FeatureId = id })
                        .ToList();
                    foreach (var f in addedFeatures)
                    {
                        v.Features.Add(f);
                    }
                });
        }
    }
}
