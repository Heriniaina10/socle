﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Socle.Models.Do
{
    public class Photo
    {
        [Key]
        public int Id { get; set; }
        [Required, StringLength(255)]
        public string FileName { get; set; }

        public int VehicleId { get; set; }
    }
}
