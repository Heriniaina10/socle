﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Socle.Models.Dto
{
    public class QueryResultDto<T>
    {
        public int Total { get; set; }
        public IEnumerable<T> Items { get; set; }
    }
}
