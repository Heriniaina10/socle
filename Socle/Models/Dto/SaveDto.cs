﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Socle.Models.Do;

namespace Socle.Models.Dto
{
    public class SaveDto
    {
        public int Id { get; set; }
        public int ModelId { get; set; }
        public bool IsRegistered { get; set; }

        [Required]
        public ContactDto Contact { get; set; }
        public DateTime LastUpdate { get; set; }
        public ICollection<int> Features { get; set; }

        public SaveDto()
        {
            Features = new Collection<int>();
        }
    }
}
