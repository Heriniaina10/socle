﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace Socle.Models.Dto
{
    public class VehicleDto
    {
        public int Id { get; set; }
        public ModelDto Model { get; set; }
        public KeyPairValueDto Make { get; set; }
        public bool IsRegistered { get; set; }
        public ContactDto Contact { get; set; }
        public DateTime LastUpdate { get; set; }
        public ICollection<KeyPairValueDto> Features { get; set; }

        public VehicleDto()
        {
            Features = new Collection<KeyPairValueDto>();
        }
    }
}
