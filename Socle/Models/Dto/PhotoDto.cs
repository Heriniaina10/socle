﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Socle.Models.Dto
{
    public class PhotoDto
    {
        public int Id { get; set; }
        public string FileName { get; set; }
    }
}
